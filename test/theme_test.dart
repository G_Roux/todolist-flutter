import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todo_list/style/Theme.dart';

void main() {
  test('Default Theme colors', () {
    final theme = MyTheme.defaultTheme;

    expect(theme.brightness, Brightness.light);
    expect(theme.accentColor.value, SecondaryColor.value);
    expect(theme.accentColorBrightness, Brightness.dark);
    expect(theme.primaryColor.value, PrimaryColor.value);
    expect(theme.primaryColorDark.value, PrimaryColorDark.value);
  });

  test('Dark Theme colors', () {
    final theme = MyTheme.darkTheme;

    expect(theme.brightness, Brightness.dark);
  });
}
