import 'package:flutter_test/flutter_test.dart';
import 'package:todo_list/extension/enum_extension.dart';
import 'package:todo_list/utils/LayoutEnum.dart';
import 'package:todo_list/utils/consts.dart';

void main() {
  test('LayoutEnum to String', () {
    expect(LayoutEnum.block.string, Consts.blockLayoutStr);
    expect(LayoutEnum.list.string, Consts.listLayoutStr);
  });
}
