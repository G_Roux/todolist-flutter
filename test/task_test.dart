import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:todo_list/provider/database_provider.dart';
import 'package:todo_list/provider/preferences_provider.dart';
import 'package:todo_list/provider/sqflite_provider.dart';
import 'package:todo_list/utils/LayoutEnum.dart';
import 'package:todo_list/utils/consts.dart';

class MockSharedPrefs implements PreferencesProvider {
  @override
  Future<String> getDisplayMode() => Future<String>.value(Consts.listLayoutStr);

  @override
  Future<bool> setDisplayMode(String value) => Future<bool>.value(true);

  @override
  Future<String> getListTitle() =>
      Future<String>.value(Consts.defaultListTitle);

  @override
  Future<bool> setListTitle(String value) => Future<bool>.value(true);
}

class MockDatabase implements DatabaseProvider {
  @override
  Future<int> createTask(TaskModel task) => Future<int>.value(task.id);

  @override
  Future<int> deleteAllTasks() => Future<int>.value(0);

  @override
  Future<int> deleteTask(int id) => Future<int>.value(1);

  @override
  Future<List<TaskModel>> readAllTasks() => Future<List<TaskModel>>.value([]);

  @override
  Future<TaskModel> readTask(int id) {
    // TODO: implement readTask
    throw UnimplementedError();
  }

  @override
  Future<List> updateMultipleTasks(List<TaskModel> tasks) =>
      Future<List>.value([]);

  @override
  Future<int> updateTask(TaskModel task) => Future<int>.value(1);
}

void main() {
  test('Initialize block with empty list', () {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    expect(_bloc.tasks, isEmpty);
  });

  test('Add task', () async {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    await _bloc.addTask(TaskModel());

    expect(_bloc.tasks.length, 1);
  });

  test('Add multiple tasks', () async {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    TaskModel myTask = TaskModel(title: 'Test', id: 1);
    TaskModel myTask2 = TaskModel(title: 'Test', id: 2);
    TaskModel myTask3 = TaskModel(title: 'Test', id: 3);

    await _bloc.addTask(myTask);
    await _bloc.addTask(myTask2);
    await _bloc.addTask(myTask3);

    expect(_bloc.tasks.length, 3);
  });

  test('Clear tasks', () async {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    await _bloc.addTask(TaskModel());
    await _bloc.clearTasks();

    expect(_bloc.tasks, isEmpty);
  });

  test('Edit task', () async {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    TaskModel myTask = TaskModel(title: 'Test', id: 1);

    await _bloc.addTask(myTask);
    expect(_bloc.tasks.first.title, 'Test');

    await _bloc.editTask(myTask.copyWith(title: 'Edit'));
    expect(_bloc.tasks.first.title, 'Edit');
  });

  test('Move task', () async {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    final myTask = TaskModel(title: 'Test', id: 1);
    final myTask2 = TaskModel(title: 'Test', id: 2);
    final myTask3 = TaskModel(title: 'Test', id: 3);

    await _bloc.addTask(myTask);
    await _bloc.addTask(myTask2);
    await _bloc.addTask(myTask3);

    expect(_bloc.tasks.first.id, 1);
    expect(_bloc.tasks.last.id, 3);

    _bloc.moveTask(0, 3);
    expect(_bloc.tasks.first.id, 2);
    expect(_bloc.tasks.last.id, 1);
  });

  test('Change Layout', () {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    expect(_bloc.layout, LayoutEnum.list);

    _bloc.changeLayout(LayoutEnum.block);
    expect(_bloc.layout, LayoutEnum.block);
  });

  test('Change list title', () {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    expect(_bloc.listTitle, 'ToDoList');

    _bloc.changeListTitle('Test');
    expect(_bloc.listTitle, 'Test');
  });

  test('Delete task', () async {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);
    final myTask = TaskModel(title: 'Test', id: 1);

    await _bloc.addTask(myTask);
    await _bloc.deleteTask(myTask);

    expect(_bloc.tasks, isEmpty);
  });

  test('TaskModel from JSON', () {
    final myTask = TaskModel.fromJson({
      SQLiteProvider.columnTitle: 'Test',
      SQLiteProvider.columnColor: Colors.green.value,
      SQLiteProvider.columnId: 1,
      SQLiteProvider.columnIndex: 0,
    });

    expect(myTask.title, 'Test');
    expect(myTask.color.value, Colors.green.value);
    expect(myTask.id, 1);
    expect(myTask.index, 0);
  });

  test('TaskModel to JSON', () {
    final myTask = TaskModel(title: 'Test', color: Colors.green, index: 0);
    final json = myTask.toJson();

    expect(json[SQLiteProvider.columnTitle], 'Test');
    expect(json[SQLiteProvider.columnColor], Colors.green.value);
    expect(json[SQLiteProvider.columnIndex], 0);
  });

  test('Dispose HomeBloc', () {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    _bloc.dispose();
  });

  test('InitState HomeBloc', () {
    final _prefs = MockSharedPrefs();
    final _database = MockDatabase();
    final _bloc = HomeBloc(_prefs, _database);

    _bloc.initState();
  });
}
