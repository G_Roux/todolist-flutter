import 'package:flutter_test/flutter_test.dart';
import 'package:todo_list/extension/string_extension.dart';
import 'package:todo_list/utils/LayoutEnum.dart';
import 'package:todo_list/utils/consts.dart';

void main() {
  test('String to LayoutEnum', () {
    expect(Consts.blockLayoutStr.toLayout(), LayoutEnum.block);
    expect(Consts.listLayoutStr.toLayout(), LayoutEnum.list);
  });
}
