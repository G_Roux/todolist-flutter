# ToDoList

[![pipeline status](https://gitlab.com/G_Roux/todolist-flutter/badges/master/pipeline.svg)](https://gitlab.com/G_Roux/todolist-flutter/-/commits/master)
[![coverage report](https://gitlab.com/G_Roux/todolist-flutter/badges/master/coverage.svg)](https://gitlab.com/G_Roux/todolist-flutter/-/commits/master)

Simple To-Do list using Material design.

# Summary

* [TODO](#todo)
* [Features](#features)
* [Installation](#installation)
* [Screenshots](#screenshots)
* [Credits](#credits)
    - [Developer](#developer)
* [Dependencies](#dependencies)

## TODO

- [x] BLoC pattern
- [x] SQLite database
- [ ] English trad
- [ ] Add animations
- [ ] Edit task
- [x] Add unit tests
- [x] [PlayStore release](https://play.google.com/store/apps/details?id=com.maniak.todo_list)

## Features

* Create tasks to remember them (they are saved locally on your phone)
* Change your layout (grid or list)
* Dark mode for your eyes
* Flat design inspired interface

## Installation

There is no version on PlayStore for now. You will need to install Flutter SDK and build the APK using ``flutter build apk`` and ``flutter install``

## Screenshots

<img src="flutter_01.png" alt="drawing" height="500"/>
<img src="flutter_02.png" alt="drawing" height="500"/>
<img src="flutter_03.png" alt="drawing" height="500"/>
<img src="flutter_04.png" alt="drawing" height="500"/>
<img src="flutter_05.png" alt="drawing" height="500"/>
<img src="flutter_06.png" alt="drawing" height="500"/>

## Credits

### Developer

[Guillaume Roux](https://gitlab.com/G_Roux)

## Packages

* [shared_preferences](https://pub.dev/packages/shared_preferences)
* [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons)
* [dynamic_theme](https://pub.dev/packages/dynamic_theme)
* [rxdart](https://pub.dev/packages/rxdart)
* [path_provider](https://pub.dev/packages/path_provider)
* [sqflite](https://pub.dev/packages/sqflite)

