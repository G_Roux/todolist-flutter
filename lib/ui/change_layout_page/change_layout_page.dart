import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/utils/LayoutEnum.dart';
import 'package:todo_list/style/Theme.dart';

class ChangeLayoutPage extends StatefulWidget {
  static const routeName = '/change-layout';

  @override
  State<StatefulWidget> createState() => _ChangeLayoutState();
}

class _ChangeLayoutState extends State<ChangeLayoutPage> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title: Text(
          "Changer l'affichage",
          style: TextStyle(fontFamily: 'Angelinatta'),
        ),
      ),
      body: SafeArea(
        child: StreamBuilder<LayoutEnum>(
          stream: _homeBloc.onLayoutChanged,
          builder: (context, snapshot) {
            return Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ChoiceChip(
                      backgroundColor: Colors.transparent,
                      shape: CircleBorder(),
                      label: Icon(Entypo.grid, size: 150),
                      selected: snapshot.data == LayoutEnum.block,
                      selectedColor:
                          Theme.of(context).brightness == Brightness.dark
                              ? DarkChipSelected
                              : Theme.of(context).chipTheme.selectedColor,
                      onSelected: (_) =>
                          _homeBloc.changeLayout(LayoutEnum.block)),
                  SizedBox(height: 16),
                  ChoiceChip(
                    shape: CircleBorder(),
                    backgroundColor: Colors.transparent,
                    selectedColor:
                        Theme.of(context).brightness == Brightness.dark
                            ? DarkChipSelected
                            : Theme.of(context).chipTheme.selectedColor,
                    label: Icon(Icons.list, size: 150),
                    selected: snapshot.data == LayoutEnum.list,
                    onSelected: (_) => _homeBloc.changeLayout(LayoutEnum.list),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
