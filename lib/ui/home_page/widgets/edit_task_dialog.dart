import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:todo_list/style/Theme.dart';

class EditTaskDialog extends StatefulWidget {
  final TaskModel taskModel;

  EditTaskDialog(this.taskModel);

  @override
  State<StatefulWidget> createState() => _EditTaskDialogState();
}

class _EditTaskDialogState extends State<EditTaskDialog> {
  final _formKey = GlobalKey<FormState>();
  final _random = Random();

  String _title;
  Color _taskColor;
  TextEditingController _controller;

  void _changeColor() {
    setState(() => _taskColor =
        MyColors.taskColor[_random.nextInt(MyColors.taskColor.length)]);
  }

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() {
    if (validateAndSave())
      Navigator.pop(
          context, widget.taskModel.copyWith(title: _title, color: _taskColor));
  }

  @override
  void initState() {
    super.initState();
    _taskColor = widget.taskModel.color;
    _title = widget.taskModel.title;
    _controller = TextEditingController(text: _title);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      title: Center(
        child: GestureDetector(
          onTap: _changeColor,
          child: Icon(Foundation.clipboard_pencil, size: 80, color: _taskColor),
        ),
      ),
      content: Form(
        key: _formKey,
        child: TextFormField(
          controller: _controller,
          textCapitalization: TextCapitalization.sentences,
          autofocus: true,
          decoration: InputDecoration(hintText: 'Editer tâche'),
          onSaved: (val) => _title = val,
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Annuler'),
          onPressed: () => Navigator.pop(context),
        ),
        FlatButton(
          child: Text('Modifier'),
          onPressed: () => validateAndSubmit(),
        ),
      ],
    );
  }
}
