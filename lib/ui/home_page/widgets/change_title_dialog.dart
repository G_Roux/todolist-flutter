import 'package:flutter/material.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';

class ChangeTitleDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChangeTitleDialogState();
}

class _ChangeTitleDialogState extends State<ChangeTitleDialog> {
  TextEditingController _controller;
  HomeBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of(context);
    _controller = TextEditingController(text: _bloc.listTitle);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      title: Text('Nouveau titre'),
      content: TextField(
        style: TextStyle(fontFamily: 'Angelinatta'),
        controller: _controller,
        autofocus: true,
      ),
      actions: [
        FlatButton(
          child: Text('Annuler'),
          onPressed: () => Navigator.pop<String>(context, ""),
        ),
        FlatButton(
          child: Text('Valider'),
          onPressed: () => Navigator.pop<String>(context, _controller.text),
        ),
      ],
    );
  }
}
