import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:todo_list/style/Theme.dart';

class AddTaskDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddTaskDialogState();
}

class _AddTaskDialogState extends State<AddTaskDialog> {
  final _formKey = GlobalKey<FormState>();
  final _random = Random();

  String _titleContent;
  Color _taskColor;
  HomeBloc _homeBloc;

  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> validateAndSubmit() async {
    if (validateAndSave()) {
      _homeBloc.addTask(TaskModel(
        title: _titleContent,
        index: _homeBloc.tasks.length,
        color: _taskColor,
      ));
      Navigator.pop(context);
    }
  }

  void _changeColor() {
    setState(() => _taskColor =
        MyColors.taskColor[_random.nextInt(MyColors.taskColor.length)]);
  }

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _taskColor = MyColors.taskColor[_random.nextInt(MyColors.taskColor.length)];
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      title: Center(
        child: GestureDetector(
          onTap: _changeColor,
          child: Icon(Foundation.clipboard_pencil, size: 80, color: _taskColor),
        ),
      ),
      content: Form(
        key: _formKey,
        child: TextFormField(
          textCapitalization: TextCapitalization.sentences,
          autofocus: true,
          decoration: InputDecoration(hintText: "Nouvelle tâche"),
          validator: (value) {
            if (value.isEmpty) return "Aucune tâche";
            return null;
          },
          onSaved: (value) => _titleContent = value,
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Fermer"),
          onPressed: () => Navigator.pop(context),
        ),
        FlatButton(
          child: Text("Ajouter"),
          onPressed: () => validateAndSubmit(),
        ),
      ],
    );
  }
}
