import 'package:flutter/material.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:todo_list/ui/home_page/widgets/edit_task_dialog.dart';

class TaskTile extends StatefulWidget {
  final TaskModel task;
  final Key key;

  TaskTile(this.task, {this.key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TaskTileState();
}

class _TaskTileState extends State<TaskTile> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Container(
        decoration: BoxDecoration(
          color: widget.task.color,
          borderRadius: BorderRadius.circular(30.0),
        ),
        child: ListTile(
          onTap: () => showDialog<TaskModel>(
                  context: context, builder: (_) => EditTaskDialog(widget.task))
              .then(_homeBloc.editTask),
          title: Text(
            widget.task.title,
            style: TextStyle(color: Colors.white),
          ),
          trailing: IconButton(
            onPressed: () => _homeBloc.deleteTask(widget.task),
            icon: Icon(Icons.delete, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
