import 'package:flutter/material.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:todo_list/ui/home_page/widgets/edit_task_dialog.dart';

class TaskCard extends StatefulWidget {
  final TaskModel task;

  TaskCard(this.task);

  @override
  State<StatefulWidget> createState() => _TaskCardState();
}

class _TaskCardState extends State<TaskCard> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => showDialog<TaskModel>(
          context: context,
          builder: (_) => EditTaskDialog(widget.task)).then(_homeBloc.editTask),
      child: Container(
        margin: EdgeInsets.all(6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(18), color: widget.task.color),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              widget.task.title,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 100,
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            IconButton(
              icon: Icon(Icons.delete),
              color: Colors.white,
              onPressed: () => _homeBloc.deleteTask(widget.task),
            ),
          ],
        ),
      ),
    );
  }
}
