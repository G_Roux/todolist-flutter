import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:share/share.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:todo_list/ui/home_page/widgets/add_task_dialog.dart';
import 'package:todo_list/ui/home_page/widgets/change_title_dialog.dart';
import 'package:todo_list/ui/home_page/widgets/task_card.dart';
import 'package:todo_list/ui/home_page/widgets/task_tile.dart';
import 'package:todo_list/ui/settings_page/settings_page.dart';
import 'package:todo_list/utils/LayoutEnum.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/';

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController _controller = TextEditingController();
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  void _shareList() {
    if (_homeBloc.tasks.isEmpty) return;
    String formattedStr = "";
    for (final e in _homeBloc.tasks) {
      formattedStr += "- ${e.title}\n";
    }
    Share.share(formattedStr);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        centerTitle: true,
        title: GestureDetector(
          onTap: () => showDialog<String>(
                  context: context, builder: (_) => ChangeTitleDialog())
              .then(_homeBloc.changeListTitle),
          child: StreamBuilder<String>(
            stream: _homeBloc.onListTitleChanged,
            builder: (_, snapshot) {
              if (!snapshot.hasData) return CircularProgressIndicator();
              return Text(
                snapshot.data,
                style: TextStyle(fontFamily: 'Angelinatta'),
              );
            },
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.share),
          onPressed: _shareList,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesome.cog),
            onPressed: () =>
                Navigator.pushNamed(context, SettingsPage.routeName),
          ),
        ],
      ),
      floatingActionButton: Theme(
        data: Theme.of(context),
        child: FloatingActionButton.extended(
          icon: Icon(
            Icons.edit,
            color: Theme.of(context).brightness == Brightness.light
                ? Colors.white
                : Colors.black,
          ),
          label: Text(
            "Ajouter tâche",
            style: TextStyle(
              fontFamily: 'Postnote',
              fontWeight: FontWeight.bold,
              letterSpacing: 1.0,
              color: Theme.of(context).brightness == Brightness.light
                  ? Colors.white
                  : Colors.black,
            ),
          ),
          onPressed: () => _goToAddTask(),
        ),
      ),
      body: SafeArea(
        child: StreamBuilder<List<TaskModel>>(
          stream: _homeBloc.onTasksChanged,
          builder: (context, snapshot) {
            return Container(
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              child: snapshot.data == null || snapshot.data.isEmpty
                  ? _buildNoTaskIcon()
                  : _buildTaskGrid(),
            );
          },
        ),
      ),
    );
  }

  Widget _buildTaskGrid() {
    return StreamBuilder<LayoutEnum>(
      stream: _homeBloc.onLayoutChanged,
      builder: (context, snapshot) {
        return snapshot.data == LayoutEnum.block
            ? StaggeredGridView.count(
                physics: BouncingScrollPhysics(),
                crossAxisCount: 2,
                children:
                    _homeBloc.tasks.map<Widget>((e) => TaskCard(e)).toList(),
                staggeredTiles: _homeBloc.tasks
                    .map<StaggeredTile>((e) => StaggeredTile.fit(1))
                    .toList(),
              )
            : ReorderableListView(
                onReorder: _homeBloc.moveTask,
                children: [
                  ..._homeBloc.tasks
                      ?.map((e) => TaskTile(e, key: ValueKey(e.id))),
                ],
              );
      },
    );
  }

  Widget _buildNoTaskIcon() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(MaterialCommunityIcons.clipboard_check_outline, size: 130),
        SizedBox(height: 16),
        Text(
          "Vous n'avez aucune tâche en cours.",
          style: TextStyle(
            fontFamily: 'Postnote',
            fontSize: 22,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).brightness == Brightness.light
                ? Colors.grey
                : Colors.white,
          ),
        ),
      ],
    );
  }

  _goToAddTask() {
    showDialog(context: context, builder: (_) => AddTaskDialog());
  }

  @override
  void dispose() {
    _controller.clear();
    super.dispose();
  }
}
