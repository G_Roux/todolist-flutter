import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/ui/change_layout_page/change_layout_page.dart';
import 'package:todo_list/utils/changeBrightness.dart';

class SettingsPage extends StatefulWidget {
  static const routeName = '/settings';

  @override
  State<StatefulWidget> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          "Options",
          style: TextStyle(
            fontFamily: 'Angelinatta',
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              _buildRowSwitch(),
              _buildRowLayout(),
              _buildRowClear(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRowSwitch() {
    return ListTile(
      onTap: () => changeBrightness(context),
      title: Text("Mode sombre"),
      trailing: Switch(
        onChanged: (bool value) => changeBrightness(context),
        value: Theme.of(context).brightness == Brightness.dark,
      ),
      leading: Icon(FontAwesome.moon_o),
    );
  }

  Widget _buildRowLayout() {
    return ListTile(
      leading: Icon(Entypo.grid),
      title: Text("Changer l'affichage"),
      trailing: Icon(Icons.keyboard_arrow_right),
      onTap: () => Navigator.pushNamed(context, ChangeLayoutPage.routeName),
    );
  }

  Widget _buildRowClear() {
    return ListTile(
      leading: Icon(Icons.clear_all),
      title: Text("Vider la liste"),
      onTap: () {
        _homeBloc.clearTasks();
        Navigator.pop(context);
      },
    );
  }
}
