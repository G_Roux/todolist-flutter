import 'package:flutter/material.dart';

class MyColors {
  static List<Color> taskColor = [
    Color(0xff1abc9c),
    Color(0xff2ecc71),
    Color(0xff3498db),
    Color(0xff9b59b6),
    Color(0xff34495e),
    Color(0xff16a085),
    Color(0xff27ae60),
    Color(0xff2980b9),
    Color(0xff8e44ad),
    Color(0xff2c3e50),
    Color(0xfff1c40f),
    Color(0xffe67e22),
    Color(0xffe74c3c),
    Color(0xff95a5a6),
    Color(0xfff39c12),
    Color(0xffd35400),
    Color(0xffc0392b),
    Color(0xff7f8c8d),
    Color(0xff00b894),
    Color(0xff00cec9),
    Color(0xff0984e3),
    Color(0xff6c5ce7),
    Color(0xfffdcb6e),
    Color(0xffe17055),
    Color(0xffd63031),
    Color(0xffe84393),
    Color(0xff636e72),
    Color(0xff2d3436),
    Color(0xfff9ca24),
    Color(0xfff0932b),
    Color(0xffeb4d4b),
    Color(0xff6ab04c),
    Color(0xff7ed6df),
    Color(0xffe056fd),
    Color(0xff686de0),
    Color(0xff30336b),
    Color(0xff95afc0),
    Color(0xff22a6b3),
    Color(0xffbe2edd),
    Color(0xff4834d4),
    Color(0xff130f40),
    Color(0xff535c68)
  ];
}

const PrimaryColor = const Color(0xFF008080);
const PrimaryColorLight = const Color(0xFF4cb0af);
const PrimaryColorDark = const Color(0xFF005354);

const SecondaryColor = const Color(0xFFb2dfdb);
const SecondaryColorLight = const Color(0xFFe5ffff);
const SecondaryColorDark = const Color(0xFF82ada9);

const Background = const Color(0xFFfffdf7);
const TextColor = const Color(0xFF004d40);
const IconColor = Colors.black;
const TitleColor = Colors.black;

const DarkBackground = const Color(0xFF222727);
const DarkChipSelected = const Color(0xFF82ada9);

class MyTheme {
  static final ThemeData defaultTheme = _buildMyTheme();
  static final ThemeData darkTheme = _buildDarkTheme();

  static ThemeData _buildMyTheme() {
    final ThemeData base = ThemeData.light();
    return base.copyWith(
      accentColor: SecondaryColor,
      accentColorBrightness: Brightness.dark,
      primaryColor: PrimaryColor,
      primaryColorDark: PrimaryColorDark,
      primaryColorLight: PrimaryColorLight,
      primaryColorBrightness: Brightness.dark,
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: SecondaryColor,
        textTheme: ButtonTextTheme.primary,
      ),
      scaffoldBackgroundColor: Background,
      cardColor: Background,
      backgroundColor: Background,
      textSelectionTheme: base.textSelectionTheme.copyWith(
        selectionColor: PrimaryColorLight,
      ),
      textTheme: base.textTheme.copyWith(
        headline6: base.textTheme.headline6.copyWith(color: TextColor),
        bodyText2: base.textTheme.bodyText2.copyWith(color: TextColor),
        bodyText1: base.textTheme.bodyText1.copyWith(color: TextColor),
      ),
      iconTheme: base.iconTheme.copyWith(color: IconColor),
      primaryIconTheme: base.primaryIconTheme.copyWith(color: IconColor),
      floatingActionButtonTheme: base.floatingActionButtonTheme
          .copyWith(foregroundColor: Colors.black),
      primaryTextTheme: base.primaryTextTheme
          .copyWith(headline6: TextStyle(color: TitleColor)),
    );
  }

  static ThemeData _buildDarkTheme() {
    final ThemeData base = ThemeData.dark();

    return base.copyWith(
      accentColor: SecondaryColor,
      accentColorBrightness: Brightness.dark,
      primaryColor: PrimaryColor,
      primaryColorDark: PrimaryColorDark,
      primaryColorLight: PrimaryColorLight,
      primaryColorBrightness: Brightness.dark,
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: SecondaryColor,
        textTheme: ButtonTextTheme.primary,
      ),
      floatingActionButtonTheme: base.floatingActionButtonTheme
          .copyWith(foregroundColor: Colors.black),
      scaffoldBackgroundColor: DarkBackground,
      cardColor: DarkBackground,
      textSelectionTheme: base.textSelectionTheme.copyWith(
        selectionColor: PrimaryColorLight,
      ),
      backgroundColor: Background,
      textTheme: base.textTheme.copyWith(
        headline6: base.textTheme.headline6.copyWith(color: TextColor),
        bodyText2: base.textTheme.bodyText2.copyWith(color: TextColor),
        bodyText1: base.textTheme.bodyText1.copyWith(color: TextColor),
      ),
    );
  }
}
