import 'package:flutter/material.dart';
import 'package:todo_list/provider/sqflite_provider.dart';
import 'package:todo_list/style/Theme.dart';
import 'dart:math';

class TaskModel {
  final String title;
  final Color color;
  final int id;
  final int index;

  TaskModel({this.title, Color color, this.id, this.index})
      : this.color = color ??
            MyColors.taskColor[Random().nextInt(MyColors.taskColor.length)];

  factory TaskModel.fromJson(Map<String, dynamic> json) => TaskModel(
        title: json[SQLiteProvider.columnTitle],
        color: Color(json[SQLiteProvider.columnColor]),
        id: json[SQLiteProvider.columnId],
        index: json[SQLiteProvider.columnIndex],
      );

  Map<String, dynamic> toJson() => {
        SQLiteProvider.columnTitle: this.title,
        SQLiteProvider.columnColor: this.color.value,
        SQLiteProvider.columnIndex: this.index,
      };

  TaskModel copyWith({String title, Color color, int id, int index}) =>
      TaskModel(
        title: title ?? this.title,
        color: color ?? this.color,
        id: id ?? this.id,
        index: index ?? this.index,
      );
}
