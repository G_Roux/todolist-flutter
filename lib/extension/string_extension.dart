import 'package:todo_list/utils/consts.dart';

import '../utils/LayoutEnum.dart';

extension StringModifier on String {
  LayoutEnum toLayout() {
    switch (this) {
      case Consts.listLayoutStr:
        return LayoutEnum.list;
      case Consts.blockLayoutStr:
      default:
        return LayoutEnum.block;
    }
  }
}
