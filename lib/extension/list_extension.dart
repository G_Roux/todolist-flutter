import 'package:todo_list/model/task_model.dart';

extension TasksListModifier on List<TaskModel> {
  List<String> toListString() =>
      List<String>.generate(this.length, (index) => this[index].title);
}
