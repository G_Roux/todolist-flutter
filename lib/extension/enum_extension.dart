import 'package:todo_list/utils/LayoutEnum.dart';
import 'package:todo_list/utils/consts.dart';

extension LayoutEnumModifier on LayoutEnum {
  String get string {
    switch (this) {
      case LayoutEnum.block:
        return Consts.blockLayoutStr;
      case LayoutEnum.list:
        return Consts.listLayoutStr;
      default:
        return null;
    }
  }
}
