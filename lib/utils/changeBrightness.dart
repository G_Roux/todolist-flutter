import 'package:flutter/material.dart';
import 'package:todo_list/utils/dynamic_theme.dart';

void changeBrightness(BuildContext context) =>
    DynamicTheme.of(context).toggleBrightness();

void changeColor(BuildContext context) {
  DynamicTheme.of(context).setThemeData(ThemeData(
      primaryColor: Theme.of(context).primaryColor == Colors.blue
          ? Colors.red
          : Colors.blue));
}
