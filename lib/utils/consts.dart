class Consts {
  static const defaultListTitle = 'ToDoList';
  static const listLayoutStr = 'list';
  static const blockLayoutStr = 'block';
}
