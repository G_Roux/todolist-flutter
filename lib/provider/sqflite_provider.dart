import 'dart:io';
import 'package:sqflite/sqflite.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:todo_list/provider/database_provider.dart';

class SQLiteProvider implements DatabaseProvider {
  static final String _dbName = 'myTasks';
  static final int _dbVersion = 2;

  static final String tableTask = 'task_table';
  static final String columnId = 'id';
  static final String columnTitle = 'title';
  static final String columnColor = 'color';
  static final String columnIndex = 'position';

  SQLiteProvider._privateContructor();

  factory SQLiteProvider() => instance;

  static final SQLiteProvider instance = SQLiteProvider._privateContructor();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _dbName);
    return openDatabase(path, version: _dbVersion, onCreate: _onCreate);
  }

  Future<void> _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableTask (
      $columnId INTEGER PRIMARY KEY,
      $columnTitle TEXT NOT NULL,
      $columnColor INTEGER NOT NULL,
      $columnIndex INTEGER NOT NULL
    )
    ''');
  }

  // CRUD operations

  @override
  Future<int> createTask(TaskModel task) async {
    final db = await instance.database;
    return await db.insert(tableTask, task.toJson());
  }

  @override
  Future<List<TaskModel>> readAllTasks() async {
    final db = await instance.database;
    return (await db.query(tableTask))
        .toList()
        .map<TaskModel>((e) => TaskModel.fromJson(e))
        .toList();
  }

  @override
  Future<TaskModel> readTask(int id) async {
    final db = await instance.database;
    List<Map> queryResults =
        await db.query(tableTask, where: '$columnId = ?', whereArgs: [id]);
    if (queryResults.isNotEmpty) return TaskModel.fromJson(queryResults.first);
    return null;
  }

  @override
  Future<int> updateTask(TaskModel task) async {
    final db = await instance.database;
    return await db.update(tableTask, task.toJson(),
        where: '$columnId = ?', whereArgs: [task.id]);
  }

  @override
  Future<List> updateMultipleTasks(List<TaskModel> tasks) async {
    assert(tasks != null);
    final db = await instance.database;
    final batch = db.batch();
    for (final e in tasks) {
      batch.update(tableTask, e.toJson(),
          where: '$columnId = ?', whereArgs: [e.id]);
    }
    return await batch.commit();
  }

  @override
  Future<int> deleteTask(int id) async {
    final db = await instance.database;
    return db.delete(tableTask, where: '$columnId = ?', whereArgs: [id]);
  }

  @override
  Future<int> deleteAllTasks() async {
    final db = await instance.database;
    return db.delete(tableTask);
  }
}
