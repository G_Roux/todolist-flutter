abstract class PreferencesProvider {
  Future<String> getDisplayMode();
  Future<bool> setDisplayMode(String value);
  Future<String> getListTitle();
  Future<bool> setListTitle(String value);
}
