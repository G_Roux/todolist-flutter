import 'package:todo_list/model/task_model.dart';

abstract class DatabaseProvider {
  Future<int> createTask(TaskModel task);
  Future<List<TaskModel>> readAllTasks();
  Future<TaskModel> readTask(int id);
  Future<int> updateTask(TaskModel task);
  Future<List> updateMultipleTasks(List<TaskModel> tasks);
  Future<int> deleteTask(int id);
  Future<int> deleteAllTasks();
}