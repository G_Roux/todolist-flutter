import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo_list/provider/preferences_provider.dart';
import 'package:todo_list/utils/consts.dart';

class SharedPrefsProvider implements PreferencesProvider {
  static final _displayModePrefs = 'displayMode';
  static final _listTitlePrefs = 'listTitle';

  SharedPrefsProvider._privateConstructor();

  factory SharedPrefsProvider() => instance;

  static final SharedPrefsProvider instance =
      SharedPrefsProvider._privateConstructor();

  static SharedPreferences _sharedPreferences;

  Future<SharedPreferences> get sharedPreferences async {
    if (_sharedPreferences != null) return _sharedPreferences;
    _sharedPreferences = await _initSharedPreferences();
    return _sharedPreferences;
  }

  Future<SharedPreferences> _initSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  @override
  Future<String> getDisplayMode() async {
    final prefs = await instance.sharedPreferences;
    return prefs.getString(_displayModePrefs) ?? Consts.listLayoutStr;
  }

  @override
  Future<bool> setDisplayMode(String value) async {
    final prefs = await instance.sharedPreferences;
    return prefs.setString(_displayModePrefs, value);
  }

  @override
  Future<String> getListTitle() async {
    final prefs = await instance.sharedPreferences;
    return prefs.getString(_listTitlePrefs) ?? Consts.defaultListTitle;
  }

  @override
  Future<bool> setListTitle(String value) async {
    final prefs = await instance.sharedPreferences;
    return prefs.setString(_listTitlePrefs, value);
  }
}
