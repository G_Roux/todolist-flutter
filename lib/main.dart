import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:todo_list/bloc/bloc.dart';
import 'package:todo_list/bloc/bloc_provider.dart';
import 'package:todo_list/bloc/home_bloc.dart';
import 'package:todo_list/provider/shared_prefs_provider.dart';
import 'package:todo_list/provider/sqflite_provider.dart';
import 'package:todo_list/ui/change_layout_page/change_layout_page.dart';
import 'package:todo_list/ui/home_page/home_page.dart';
import 'package:todo_list/style/Theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:todo_list/ui/settings_page/settings_page.dart';
import 'package:todo_list/utils/dynamic_theme.dart';

void main() {
  final _sharedPrefs = SharedPrefsProvider();
  final _database = SQLiteProvider();
  runApp(
    BlocProvider(
      child: MyApp(),
      key: GlobalKey(),
      blocs: <BlocBase>[HomeBloc(_sharedPrefs, _database)],
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return DynamicTheme(
      defaultBrightness: SchedulerBinding.instance.window.platformBrightness,
      data: (brightness) => brightness == Brightness.light
          ? MyTheme.defaultTheme
          : MyTheme.darkTheme,
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          localizationsDelegates: [
            GlobalWidgetsLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate
          ],
          supportedLocales: [
            const Locale('en'),
            const Locale('fr'),
          ],
          title: 'ToDoList',
          debugShowCheckedModeBanner: false,
          theme: theme,
          initialRoute: HomePage.routeName,
          routes: {
            HomePage.routeName: (context) => HomePage(),
            SettingsPage.routeName: (context) => SettingsPage(),
            ChangeLayoutPage.routeName: (context) => ChangeLayoutPage(),
          },
        );
      },
    );
  }
}
