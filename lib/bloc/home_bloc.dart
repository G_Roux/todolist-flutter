import 'package:rxdart/rxdart.dart';
import 'package:todo_list/bloc/bloc.dart';
import 'package:todo_list/model/task_model.dart';
import 'package:todo_list/provider/database_provider.dart';
import 'package:todo_list/provider/preferences_provider.dart';
import 'package:todo_list/utils/LayoutEnum.dart';
import 'package:todo_list/extension/enum_extension.dart';
import 'package:todo_list/extension/string_extension.dart';
import 'package:todo_list/utils/consts.dart';

class HomeBloc implements BlocBase {
  final PreferencesProvider sharedPreferences;
  final DatabaseProvider database;

  HomeBloc(this.sharedPreferences, this.database);

  final _tasksController = BehaviorSubject<List<TaskModel>>.seeded([]);
  Stream<List<TaskModel>> get onTasksChanged => _tasksController.stream;
  List<TaskModel> get tasks => _tasksController.value;

  final _layoutController = BehaviorSubject<LayoutEnum>.seeded(LayoutEnum.list);
  Stream<LayoutEnum> get onLayoutChanged => _layoutController.stream;
  LayoutEnum get layout => _layoutController.value;

  final _listTitleController =
      BehaviorSubject<String>.seeded(Consts.defaultListTitle);
  Stream<String> get onListTitleChanged => _listTitleController.stream;
  String get listTitle => _listTitleController.value;

  @override
  void dispose() {
    _layoutController.close();
    _tasksController.close();
    _listTitleController.close();
  }

  @override
  void initState() async {
    _layoutController.sink
        .add((await sharedPreferences.getDisplayMode()).toLayout());
    final dbTasks = await database.readAllTasks();
    dbTasks.sort((a, b) => a.index.compareTo(b.index));
    _tasksController.sink.add(dbTasks);

    final title = await sharedPreferences.getListTitle();
    _listTitleController.sink.add(title);
  }

  void changeLayout(LayoutEnum newLayout) {
    if (newLayout != layout) {
      _layoutController.sink.add(newLayout);
      sharedPreferences.setDisplayMode(layout.string);
    }
  }

  Future<void> deleteTask(TaskModel task) async {
    List<TaskModel> currentTasks = tasks;
    currentTasks.removeWhere((e) => e.id == task.id);
    _tasksController.sink.add(currentTasks);
    await database.deleteTask(task.id);
  }

  Future<void> addTask(TaskModel task) async {
    List<TaskModel> currentTasks = tasks;
    task = task.copyWith(id: await database.createTask(task));
    currentTasks.add(task);
    _tasksController.sink.add(currentTasks);
  }

  Future<void> editTask(TaskModel task) async {
    if (task == null) return;
    List<TaskModel> currentTasks = tasks;
    await database.updateTask(task);
    final index = currentTasks.indexWhere((e) => e.id == task.id);
    currentTasks[index] = task;
    _tasksController.sink.add(currentTasks);
  }

  Future<void> clearTasks() async {
    _tasksController.sink.add([]);
    await database.deleteAllTasks();
  }

  void moveTask(int oldIndex, int newIndex) {
    assert(oldIndex != newIndex && newIndex < tasks.length + 1);
    List<TaskModel> newTasks = List<TaskModel>.from(tasks);
    final elemToMove = newTasks.elementAt(oldIndex);
    newTasks.removeAt(oldIndex);
    if (newIndex > oldIndex) newIndex -= 1;
    if (newIndex < 0) newIndex = 0;
    if (newIndex > newTasks.length) newIndex = newTasks.length;
    newTasks.insert(newIndex, elemToMove);
    for (int i = 0; i < newTasks.length; i++)
      newTasks[i] = newTasks[i].copyWith(index: i);
    _tasksController.sink.add(newTasks);
    database.updateMultipleTasks(newTasks);
  }

  void changeListTitle(String newTitle) {
    if (newTitle == null || newTitle.isEmpty) return;
    _listTitleController.sink.add(newTitle);
    sharedPreferences.setListTitle(newTitle);
  }
}
